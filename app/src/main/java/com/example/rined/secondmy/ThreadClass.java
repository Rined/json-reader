package com.example.rined.secondmy;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import io.realm.Realm;

/**
 * Created by Rined on 04.06.2015.
 */
public class ThreadClass implements Runnable {
    Thread th;
    StringBuilder bld = null;
    Message msg;
    Handler h;
    Context context;
    Realm realm;

    parsedObject[] parsMas;

    public ThreadClass(Handler h,Context context){
        this.context=context;
        this.h=h;
        th=new Thread(this);
        th.start();

    }
    @Override
    public void run() {
        URL url;
        InputStreamReader rd = null;
        ArrayList<String> arrList=new ArrayList<>();


        try {
            url = new URL("http://formatio.ru/api/v1/cities");
            URLConnection conn = url.openConnection();
            rd = new InputStreamReader(conn.getInputStream());
            char a;
            bld = new StringBuilder();
            while ((a = (char) rd.read()) != (char) -1)
                bld.append(a);
            JSONpars();

            for(parsedObject pOb:realm.where(parsedObject.class).findAll())
                arrList.add(pOb.getCity() + " " + pOb.getId());
            msg=h.obtainMessage(0,arrList);
            h.sendMessage(msg);
//            h.sendEmptyMessage(0);

        } catch (Throwable e) {
            msg=h.obtainMessage(0,"Error");
            h.sendMessage(msg);
        }
    }

    public void JSONpars(){
        try
        {

            JSONArray citArr = new JSONObject(bld.toString()).getJSONArray("cities");
//            parsMas=new parsedObject[citArr.length()];

            realm= Realm.getInstance(context);

            for (int i=0; i<citArr.length(); i++)
            {


                realm.beginTransaction();
                realm.copyToRealm(new parsedObject(citArr.getJSONObject(i).getInt("id"),citArr.getJSONObject(i).getString("name")));
//                parsMas[i]=new parsedObject(cur.getInt("id"),cur.getString("name"));
                realm.commitTransaction();
            }

        }
        catch (Exception e) {}

    }




}
