package com.example.rined.secondmy;

import io.realm.RealmObject;

/**
 * Created by Rined on 04.06.2015.
 */
public class parsedObject extends RealmObject{

    private int id;
    private String city;

    public parsedObject(){}
    public parsedObject(int id,String city){
        this.id=id;
        this.city=city;
    }

    public int getId(){
        return this.id;
    }

    public String getCity(){
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setId(int id) {
        this.id = id;
    }


}
