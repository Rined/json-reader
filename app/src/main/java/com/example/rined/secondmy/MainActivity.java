package com.example.rined.secondmy;

import android.app.Activity;
import android.os.Handler;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class MainActivity extends Activity {
    private ListView txt;
    Handler h;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt = (ListView)findViewById(R.id.listView1);

        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
//                ArrayList<String> arrList=new ArrayList<>();
//                for(parsedObject pOb:(parsedObject[])msg.obj)
//                    arrList.add(pOb.toString());
//                String[] str=arrList.toArray(new String[arrList.size()]);
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1, str);
//                txt.setAdapter(adapter);
//                Realm realm=(Realm)msg.obj;
//                RealmResults<parsedObject> result=realm.where(parsedObject.class).findAll();
//                for(parsedObject pOb:realm.where(parsedObject.class).findAll())
//                    arrList.add(pOb.getCity() + " " + pOb.getId());

//                String[] str=arrList.toArray(new String[arrList.size()]);
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1, arrList);
                txt.setAdapter(new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,(ArrayList)msg.obj));

            };
        };


        ThreadClass cls=new ThreadClass(h,this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
